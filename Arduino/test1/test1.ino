#include <Servo.h>

int r = 5;     // D1
int g = 16;    // D0
int b = 4;     // D2

int level_r, level_g, level_b;

int color;  // 0 : r
            // 1 : g
            // 2 : b

int buzzer = 0; // D3

int servo = 14; // D5
Servo motor;

/*
 * setup s'exécute une seule fois
 * On met ici les paramètres de démarrage
 */
void setup() {
  // lie la 
  pinMode(r, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(b, OUTPUT);

  level_r = 255;
  color = level_g = level_b = 0;

  // lie la pin à celui du moteur
  motor.attach(servo);

  // lie la pin au buzzer
  pinMode(buzzer, OUTPUT);
}

/*
 * Boucle du programme
 * C'est qu'une boucle while (true) dernière grosso modo
 */
void loop() {
  if (level_r == 255) {
    color = 1;
    motor.write(0);
    digitalWrite(buzzer, HIGH);
  } else if (level_g == 255) {
    color = 2;
    motor.write(90);
    digitalWrite(buzzer, HIGH);
  } else if (level_b == 255) {
    color = 0;
    motor.write(180);
    digitalWrite(buzzer, HIGH);
  }
  
  switch (color) {
    case 0:
    level_b--;
    level_r++;
    break;
    
    case 1:
    level_r--;
    level_g++;
    break;
    
    case 2:
    level_g--;
    level_b++;
    break;
  }

  analogWrite(r, level_r);
  analogWrite(g, level_g);
  analogWrite(b, level_b);
  
  delay(16);
  digitalWrite(buzzer, LOW);
}

