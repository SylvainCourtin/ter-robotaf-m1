package fr.orleans.info.robotaf.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class WIFIClient {

    public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        
        DataInputStream dataInputStream;
        DataOutputStream dataOutputStream;
        String send;
        boolean finished = false;
        
        Socket client = new Socket("192.168.4.1", 8080);
        dataInputStream = new DataInputStream(client.getInputStream());
        dataOutputStream = new DataOutputStream(client.getOutputStream());
        
        while (!finished) {
            send = sc.nextLine();
            
            if (send.equals("exit")) {
                finished = true;
            } else {
                dataOutputStream.writeBytes(send);
                dataOutputStream.flush();
                
                //System.out.println(dataInputStream.readByte());
            }
        }
        
        dataInputStream.close();
        dataOutputStream.close();
        client.close();
        
        sc.close();
    }

}
