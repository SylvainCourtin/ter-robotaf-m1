#include <Servo.h>

int r = 5;     // D1
int g = 16;    // D0
int b = 4;     // D2

int buzzer = 2; // D4

// définition des broches utilisées
int trig = 0;   // D3
int echo = 14;   // D5
long lecture_echo;
long cm;

int servo = 12;   // D6
Servo motor;

void setup()
{
  pinMode(r, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(b, OUTPUT);

  pinMode(buzzer, OUTPUT);

  motor.attach(servo);
  
  pinMode(trig, OUTPUT);
  digitalWrite(trig, LOW);
  pinMode(echo, INPUT);
  Serial.begin(9600);
}

void loop()
{
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  lecture_echo = pulseIn(echo, HIGH);
  cm = lecture_echo / 58;
  Serial.print("Distance en cm : ");
  Serial.println(cm);

  if (cm > 50) {
    analogWrite(r, 0);
    analogWrite(g, 0);
    analogWrite(b, 512);

    motor.write(180);

    digitalWrite(buzzer, LOW);
    delay(1000);
  } else if (cm <= 50 && cm >= 10) {
    analogWrite(r, 0);
    analogWrite(g, 512);
    analogWrite(b, 0);

    digitalWrite(buzzer, HIGH);
    delay(50);
    digitalWrite(buzzer, LOW);
    delay(500);

    motor.write(100);
  } else {
    analogWrite(r, 512);
    analogWrite(g, 0);
    analogWrite(b, 0);

    digitalWrite(buzzer, HIGH);
    delay(50);
    digitalWrite(buzzer, LOW);
    delay(100);

    motor.write(90);
  }
}
