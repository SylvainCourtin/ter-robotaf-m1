/*
 * Pin pour tous les composants branchés sur l'ESP8266
 */
#include <Servo.h>

/* Disque de led
 * D0 : G
 * D1 : R
 * D2 : B
 */
 int r = 5, g = 16, b = 4;

/*
 * Pin du buzzer
 * D5 : buzzer
 */
int buzzer = 14;

/* Pin de l'ultra son
 * D6 : echo
 * D7 : trig
 */
int echo = 12, trig = 13;
long lecture_echo;
long cm;

/* Pin du servomoteur contrôlant l'ultra son
 * D8 : usMotor
 */
int usMotor = 15;
Servo USMotor;

/*  Pin du servomoteur gauche et droite
 *  D3 : LMotor
 *  D4 : RMotor
 */
int lMotor = 0, rMotor = 2;
Servo motorLeft, motorRight;

/*
 * Partie WiFi
 */
#include <ESP8266WiFi.h>

const char* ssid = "RoboTaf ESP8266";
const char* password = "RoboTaf2017";

WiFiServer server(8080);
WiFiClient client;

int timeBuzzer = -1;
String help;

void setup() {
  Serial.begin(9600);
  Serial.println("Initialize...");

  // lier chaque pins aux composants
  Serial.print("Pin... ");

  // disque de led
  pinMode(r, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(b, OUTPUT);

  // buzzer
  pinMode(buzzer, OUTPUT);

  // ultra son
  pinMode(trig, OUTPUT);
  digitalWrite(trig, LOW);
  pinMode(echo, INPUT);

  // servomoteur
  USMotor.attach(usMotor);
  motorLeft.attach(lMotor);
  motorRight.attach(rMotor);
  
  Serial.println("OK");

  // fixe le ssid et password de l'esp
  Serial.println("Configuring access point...");
  WiFi.softAP(ssid, password);
  WiFi.mode(WIFI_AP);

  // adresse IP de l'esp
  Serial.print("IP Address : ");
  Serial.println(WiFi.softAPIP());
  Serial.println("\nAccess Point : OK");

  // chargement du serveur
  Serial.print("\nStarting server...");
  server.begin();

  // message de la commande help
  help = String("Commande :");
  help += "\navancer() : avance le robot";
  help += "\nreculer() : recule le robot";
  help += "\ngauche() : tourne le robot a gauche";
  help += "\ndroite() : tourne le robot a droite";
  help += "\nklaxon() : active le klaxon";
  help += "\nleftUSMotor() : tourne le servo moteur de l'ultra son a gauche";
  help += "\nrightUSMotor() : tourne le servo moteur de l'ultra son a droite";
  help += "\nresetPositionUSMotor() : remet le servo moteur de l'ultra son a sa position d'origine";
  help += "\nstopRobot() : arrete le robot";
  help += "\nconnectedLed() : couleur de la led lorsque le robot est connecte a l'application";
  help += "\nerrorLed() : couleur de la led lorsqu'il y a un probleme";
  help += "\ngetValueUltrasonicSensor() : retourne la distance entre le robot et l'obstacle";
  help += "\ngetVersion() : retourne la version du microprogramme";
  Serial.println(" Ready");

  stopRobot();
}

void loop() {
  String req, str;
  int bytes;

  analogWrite(r, 0);
  analogWrite(g, 0);
  analogWrite(b, 255);

  // connexion d'un client
  client = server.available();

  // si un client se connecte
  if (client) {
    Serial.println("Client connected");

    connectedLed();

    // Tant que le client est connecté, on reste dans la boucle
    while (client.connected()) {
  
      digitalWrite(trig, HIGH);
      delayMicroseconds(10);
      digitalWrite(trig, LOW);
      lecture_echo = pulseIn(echo, HIGH);
      cm = lecture_echo / 58;
      
      // lecture du message reçu
      while (client.available()) {
        bytes = client.read();
        req += (char) bytes;
      }

      if (req == "avancer()") {
        avancer();
      } 
      else if (req == "reculer()") {
        reculer();
      } 
      else if (req == "gauche()") {
        gauche();
      } 
      else if (req == "droite()") {
        droite();
      } 
      else if (req == "arreter()") {
        arreter();
      } 
      else if (req == "klaxon()") {
        klaxon();
      } 
      else if (req == "leftUSMotor()") {
        leftUSMotor();
      }
      else if (req == "rightUSMotor()") {
        rightUSMotor();
      }
      else if (req == "resetPositionUSMotor()") {
        resetPositionUSMotor();
      }
      else if (req == "stopRobot()") {
        stopRobot();
      }
      else if (req == "connectedLed()") {
        connectedLed();
      }
      else if (req == "errorLed()") {
        errorLed();
      }
      else if (req == "getValueUltrasonicSensor()") {
       str = String(cm);
       client.write(str.c_str());
      }
      else if (req == "getVersion()") {
       client.write("RoboTaf Arduino Version 1.1.2c");
      }
      else if (req == "help()") {
       client.write(help.c_str());
      }
      else if (req == " ") {
        // on ne fait rien, message tronqué
      }
      else if (req != "") {
        errorLed();
        client.write("Error : command not found !");
      }

      if (millis() >= timeBuzzer + 500) {
        digitalWrite(buzzer, LOW);
        timeBuzzer = -1;
      }

      if (cm <= 3) {
        errorLed();
        stopRobot();
      }

      req = "";
    }

    // le client s'est déconnecté
    client.stop();
    stopRobot();
    Serial.println("Client disconnected");
  }
}

/*
 * motorLeft : 
 *      0   : avancer
 *      180 : reculer
 *      
 * motorRight : 
 *      0   : reculer
 *      180 : avancer
 */

/**
 * Avance le robot
 */
void avancer() {
  resetPositionUSMotor();
  motorLeft.write(0);
  motorRight.write(180);
}

/**
 * Recule le robot
 */
void reculer() {
  connectedLed();
  motorLeft.write(110);
  motorRight.write(70);
}

/**
 * Tournes le robot à gauche
 */
void gauche() {
  connectedLed();
  leftUSMotor();
  motorLeft.write(100);
  motorRight.write(110);
}

/**
 * Tournes le robot à droite
 */
void droite() {
  connectedLed();
  rightUSMotor();
  motorLeft.write(80);
  motorRight.write(70);
}

/**
 * Arrête le robot
 */
void arreter() {
  connectedLed();
  motorLeft.write(90);
  motorRight.write(90);
}

/**
 * Active le klaxon
 */
void klaxon() {
  connectedLed();
  if (timeBuzzer == -1) {
    timeBuzzer = millis();
    digitalWrite(buzzer, HIGH);
  }
}

/**
 * Tourne le servomoteur de l'ultrason à gauche
 */
void leftUSMotor() {
  USMotor.write(180);
}

/**
 * Met le servomoteur de l'ultrason à sa position initiale
 */
void resetPositionUSMotor() {
  USMotor.write(90);
}

/**
 * Tourne le servomoteur de l'ultrason à droite
 */
void rightUSMotor() {
  USMotor.write(0);
}

/**
 * Change la couleur des leds en vert
 */
void connectedLed() {
  analogWrite(r, 0);
  analogWrite(g, 255);
  analogWrite(b, 0);
}

/**
 * Change la couleur des leds en rouge
 */
void errorLed() {
  analogWrite(r, 255);
  analogWrite(g, 0);
  analogWrite(b, 0);
}

/**
 * Arrête le robot par sécurité
 */
void stopRobot() {
  USMotor.write(90);
  motorLeft.write(90);
  motorRight.write(90);

  digitalWrite(buzzer, LOW);
}

