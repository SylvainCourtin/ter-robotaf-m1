package orleans.info.fr.robotaf.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import orleans.info.fr.robotaf.R;
import orleans.info.fr.robotaf.activities.net.TCPClient;
import orleans.info.fr.robotaf.activities.rtscript.RTScript;


/**
 * Created by Romain on 01/04/2017.
 */

public class TerminalActivity extends AppCompatActivity{

    private ScrollView scrollView;
    private TextView textView;
    private EditText command;
    private Button send;

    // RoboTaf Script
    private RTScript rtScript;
    private List<String> expr;

    private int conditionOrLoop = 0;

    // met à jour le textView
    private Handler update;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rtScript = new RTScript();
        expr = new LinkedList<String>();

        setContentView(R.layout.activity_terminal);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

        textView = (TextView) findViewById(R.id.commandsendrecv);
        command = (EditText) findViewById(R.id.command);
        send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // on récupère la commande à envoyer
                String request = command.getText().toString();

                // si le message n'est pas vide, on envoie
                if (RTScript.isFunctionEsp(request) && conditionOrLoop == 0) {
                    RoboTaf.tcpClient.send(request);
                    RoboTaf.commandSendRecv.add(">>>" + request);
                } else {
                    expr.add(request);

                    // si on crée une condition ou une boucle, on ajoute dans la liste
                    if (rtScript.isCondition(request) || rtScript.isLoop(request) && conditionOrLoop >= 0) {
                        RoboTaf.commandSendRecv.add(">>>" + request);
                        conditionOrLoop++;
                    }
                    // une fois qu'on a terminé la boucle, on envoie le tout au parseur
                    else if (request.equals("end") && conditionOrLoop > 0) {
                        conditionOrLoop--;

                        if (conditionOrLoop == 0) {
                            RoboTaf.commandSendRecv.add(">>>" + request);
                            rtScript.read(expr);
                            expr.clear();
                        } else {
                            RoboTaf.commandSendRecv.add("###" + request);
                        }
                    }
                    // si c'est juste un ajout de variable
                    else if (conditionOrLoop == 0) {
                        RoboTaf.commandSendRecv.add(">>>" + request);
                        rtScript.read(expr);
                        expr.clear();
                    }
                    // si on ajoute une valeur dans la liste
                    else {
                        RoboTaf.commandSendRecv.add("###" + request);
                    }
                }

                // on vide le champ EditText
                command.setText("");

                // mise à jour du textView
                updateTextView();
            }
        });

        // on change la méthode de réception des messages (pour une mise à jour continu)
        RoboTaf.tcpClient.setRecv(new TCPClient.Recv() {
            @Override
            public void recv(String str) {
                // on ajoute dans la liste le message reçu
                RoboTaf.commandSendRecv.add(str);

                // on appelle le Handler pour faire le lien entre l'application et le thread
                update.sendMessage(new Message());
            }
        });

        // permet la mise à jour du TextView
        update = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                updateTextView();
            }
        };

        updateTextView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // on remet comme avant la méthode de réception
        RoboTaf.tcpClient.setRecv(new TCPClient.Recv() {
            @Override
            public void recv(String str) {
                RoboTaf.commandSendRecv.add(str);
            }
        });
    }

    /**
     * Met à jour le textView
     */
    public void updateTextView() {
        // on supprime ce qu'il y avait avant
        textView.setText("");

        Iterator<String> iterator = RoboTaf.commandSendRecv.listIterator();

        while (iterator.hasNext()) {
            textView.append(iterator.next() + "\n");
        }
    }
}
