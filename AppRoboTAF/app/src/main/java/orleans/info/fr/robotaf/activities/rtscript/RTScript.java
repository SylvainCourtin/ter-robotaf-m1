package orleans.info.fr.robotaf.activities.rtscript;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import orleans.info.fr.robotaf.activities.RoboTaf;

/**
 * Created by David on 21/05/2017.
 *
 * RoboTaf Script
 */

public class RTScript {

    private Map<String, Double> varNumber = new HashMap<String, Double>();

    // fonction de l'esp
    private static List<String> functionEsp = new ArrayList<String>();

    static {
        functionEsp.add("avancer()");
        functionEsp.add("reculer()");
        functionEsp.add("gauche()");
        functionEsp.add("droite()");
        functionEsp.add("klaxon()");
        functionEsp.add("leftUSMotor()");
        functionEsp.add("rightUSMotor()");
        functionEsp.add("resetPositionUSMotor()");
        functionEsp.add("stopRobot()");
        functionEsp.add("connectedLed()");
        functionEsp.add("errorLed()");
        functionEsp.add("getValueUltrasonicSensor()");
        functionEsp.add("getVersion()");
        functionEsp.add("help()");
    }

    public RTScript() {

    }

    /**
     * Lecture du contenu de la liste
     * @param list
     */
    public void read(List<String> list) {
        String firstLine = list.get(0);

        try {
            // si c'est une condition if
            if (isCondition(firstLine)) {
                conditionIf(list);
            }
            // si c'est une condition while
            else if (isLoop(firstLine)) {
                conditionLoop(list);
            }
            // si c'est autre chose
            else {
                variable(list);
            }
        } catch (NullPointerException npe) {
            RoboTaf.commandSendRecv.add("Error : variable not initialized");
        }
    }

    /**
     * Lecture de la condition if
     * @param list
     */
    private void conditionIf(List<String> list) {
        String firstLine = list.get(0);
        list.remove(list.get(list.size() - 1));
        list.remove(0);

        // on retire le if, then et les espaces
        firstLine = firstLine.replace("if", "").replace("then", "").replace(" ", "");

        // si la condition est vrai
        if (bExpr(firstLine)) {

            // on cherche s'il y a un else
            if (list.contains("else")) {
                // on supprime tous ce qui est après le else
                while (!list.get(list.size() - 1).contains("else")) {
                    list.remove(list.size() - 1);
                }

                // pour supprimer le else
                list.remove(list.size() - 1);
            }

            variable(list);
        }
        // condition fausse
        else if (list.contains("else") || list.contains("elseif")) {
            // on supprime l'expression du if
            while (!list.get(0).equals("else") && !list.get(0).contains("elseif")) {
                list.remove(0);
            }

            // on teste si c'est un elseif
            if (list.get(0).contains("elseif")) {
                //on vire le else du elseif
                list.set(0, list.get(0).replace("elseif", "if"));

                // on réappelle conditionIf
                conditionIf(list);
            }
            // c'est un else, alors on exécute
            else {
                list.remove(0);
                variable(list);
            }
        }
    }

    /**
     * Lecture de la condition while
     * @param list
     */
    private void conditionLoop(List<String> list) {
        String firstLine = list.get(0);
        list.remove(list.get(list.size() - 1));
        list.remove(0);

        // on retire le while, do et les espaces
        firstLine = firstLine.replace("while", "").replace("do", "").replace(" ", "");

        // on exécute la boucle
        while (bExpr(firstLine)) {
            variable(list);
        }
    }

    /**
     * Gestion de variable
     * @param list
     */
    private void variable(List<String> list) {
        // on lit chaque ligne
        for (String str : list) {

            // si on lit une fonction de l'esp
            if (isFunctionEsp(str)) {
                RoboTaf.tcpClient.send(str);
            }
            // si on lit une condition ou une boucle
            else if (isLoop(str) || isCondition(str)) {
                return ;
            }
            // on affiche un calcul
            else if ((str.contains("+") || str.contains("-") || str.contains("*") || str.contains("/"))
                    && !str.contains("=")) {
                RoboTaf.commandSendRecv.add(Double.toString(value(str)));
            }
            // on affiche une expression booléen
            else if (str.contains("<=") || str.contains("<") || str.contains(">=") || str.contains(">")
                    || str.contains("==") || str.contains("!=")) {
                RoboTaf.commandSendRecv.add(Boolean.toString(bExpr(str)));
            }
            // on ajoute une valeur à une variable
            else if (str.contains("=")) {
                addVar(str.substring(0, str.indexOf("=")).replaceAll(" ", ""), value(str.substring(str.indexOf("=") + 1)));
            }
            // on veut afficher une valeur
            else if (str.contains("print")) {
                str = str.replace("print", "").replaceAll(" ", "");
                printValue(str);
            }
            // si c'est une commande qui n'existe pas, on arrête la lecture
            else {
                RoboTaf.commandSendRecv.add("Error : command not found !");

                return;
            }
        }
    }

    /**
     * Evalues une expression
     * @param str
     * @return
     */
    private double value(String str) {
        // on retire les espaces par sécurité
        str = str.replaceAll(" ", "");

        // on lit le type d'expression (addition, soustraction, multiplication, division)
        if (str.contains("/")) {
            return value(str.substring(0, str.indexOf("/"))) / value(str.substring(str.indexOf("/") + 1));
        } else if (str.contains("*")) {
            return value(str.substring(0, str.indexOf("*"))) * value(str.substring(str.indexOf("*") + 1));
        } else if (str.contains("-")) {
            return value(str.substring(0, str.indexOf("-"))) - value(str.substring(str.indexOf("-") + 1));
        } else if (str.contains("+")) {
            return value(str.substring(0, str.indexOf("+"))) + value(str.substring(str.indexOf("+") + 1));
        }

        try {
            // on retourne la valeur
            return Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            // ou la valeur d'une variable
            return varNumber.get(str);
        }
    }

    /**
     * Evalue une expression booléenne
     * @param str
     * @return
     */
    private boolean bExpr(String str) {
        // on retire tous les espaces
        str = str.replaceAll(" ", "");

        // on lit le type d'expression booléen (<=, <, >=, >, !=, ==)
        if (str.contains("<=")) {
            return value(str.substring(0, str.indexOf("<="))) <= value(str.substring(str.indexOf("<=") + 2));
        }
        else if (str.contains("<")) {
            return value(str.substring(0, str.indexOf("<"))) < value(str.substring(str.indexOf("<") + 1));
        }
        else if (str.contains(">=")) {
            return value(str.substring(0, str.indexOf(">="))) >= value(str.substring(str.indexOf(">=") + 2));
        }
        else if (str.contains(">")) {
            return value(str.substring(0, str.indexOf(">"))) > value(str.substring(str.indexOf(">") + 1));
        }
        else if (str.contains("==")) {
            return value(str.substring(0, str.indexOf("=="))) == value(str.substring(str.indexOf("==") + 2));
        }
        else if (str.contains("!=")) {
            return value(str.substring(0, str.indexOf("!="))) != value(str.substring(str.indexOf("!=") + 2));
        }
        // si c'est true enfaite...
        else if (str.equals("true")) {
            return true;
        }

        return false;
    }

    /**
     * Ajoute une valeur dans une variable
     * @param name
     * @param value
     */
    private void addVar(String name, double value) {
        varNumber.put(name, value);
    }

    /**
     * Testes si c'est une fonction de l'esp
     * @param str
     * @return
     */
    public static boolean isFunctionEsp(String str) {
        return functionEsp.contains(str);
    }

    /**
     * testes si c'est une condition
     * @param str
     * @return
     */
    public boolean isCondition(String str) {
        return str.contains("if") && str.contains("then") && !str.contains("elseif");
    }

    /**
     * testes si c'est une boucle
     * @param str
     * @return
     */
    public boolean isLoop(String str) {
        return str.contains("while") && str.contains("do");
    }

    /**
     * Affiche la valeur d'une variable
     * @param str
     */
    public void printValue(String str) {
        if (varNumber.containsKey(str)) {
            RoboTaf.commandSendRecv.add(varNumber.get(str).toString());
        } else {
            RoboTaf.commandSendRecv.add("Error : variable not initialized");
        }
    }
}
