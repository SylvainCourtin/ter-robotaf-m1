package orleans.info.fr.robotaf.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;

import orleans.info.fr.robotaf.R;
import orleans.info.fr.robotaf.activities.net.TCPClient;


/**
 * Created by Romain on 01/04/2017.
 */

public class StartActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String PREFS = "PREFS";
    private static final String PREFS_NAME = "PREFS_NAME";
    private static final String PREFS_IP = "PREFS_IP";
    private static final String PREFS_PORT = "PREFS_PORT";
    private static final String PREFS_CONNECT = "PREFS_CONNECT";
    private static final String PREFS_LANGUAGE = "PREFS_LANGUAGE";
    private static final String PREFS_THEME_PROG = "PREFS_THEME_PROG";
    SharedPreferences sharedPreferences;

    private ImageButton control, prog, terminal, setting, connexion;

    private Handler connexionOK;
    private Handler connexionKO;
    private Handler connexionFailed;

    @Override
    protected void onCreate(Bundle savedInstanceSatate){
        super.onCreate(savedInstanceSatate);
        setContentView(R.layout.activity_start);
        sharedPreferences = getBaseContext().getSharedPreferences(PREFS, MODE_PRIVATE);
        control = (ImageButton)findViewById(R.id.control);
        prog = (ImageButton)findViewById(R.id.prog);
        terminal = (ImageButton)findViewById(R.id.terminal);
        setting = (ImageButton)findViewById(R.id.setting);
        connexion = (ImageButton)findViewById(R.id.connexion);
        control.setOnClickListener(this);
        prog.setOnClickListener(this);
        terminal.setOnClickListener(this);
        setting.setOnClickListener(this);
        connexion.setOnClickListener(this);

        if (sharedPreferences.contains(PREFS_NAME)) {
            String name = sharedPreferences.getString(PREFS_NAME, null);
            Log.v("this", " name: " + name);
        }else {
            final EditText edittext = new EditText(StartActivity.this);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            AlertDialog alertDialog;
            alertDialogBuilder.setTitle(R.string.demandeNom);

            // set dialog message
            alertDialogBuilder
                    .setView(edittext)
                    .setCancelable(false)
                    .setPositiveButton(R.string.okNom,new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //What ever you want to do with the value
                            String YouEditTextValue = edittext.getText().toString();
                            sharedPreferences
                                    .edit()
                                    .putString(PREFS_NAME, YouEditTextValue)
                                    .putString(PREFS_IP, "192.168.4.1")
                                    .putString(PREFS_LANGUAGE, "Français")
                                    .putString(PREFS_THEME_PROG, "Tableau Noir")
                                    .putInt(PREFS_PORT, 8080)
                                    .putBoolean(PREFS_CONNECT, false)
                                    .apply();
                            dialog.cancel();
                        }
                    });

            alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

        }

        connexionOK = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                connexion.setImageResource(R.drawable.connexionok);
                Toast toast = Toast.makeText(StartActivity.this, R.string.connexionOK, Toast.LENGTH_SHORT);
                toast.show();
            }
        };

        connexionKO = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                connexion.setImageResource(R.drawable.connexionko);
                Toast toast = Toast.makeText(StartActivity.this, R.string.connexionKO, Toast.LENGTH_SHORT);
                toast.show();
            }
        };

        connexionFailed = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                connexion.setImageResource(R.drawable.connexionko);
                Toast toast = Toast.makeText(StartActivity.this, R.string.connexionFailed, Toast.LENGTH_SHORT);
                toast.show();
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // déconnexion de l'esp si ce n'est pas déjà fait
        RoboTaf.tcpClient.disconnect();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.control:
                intent = new Intent(this, ControlActivity.class);
                break;
            case R.id.prog:
                intent = new Intent(this, ProgActivity.class);
                Log.v("Label","Entrer dans le mode programmation");
                break;
            case R.id.terminal:
                intent = new Intent(this, TerminalActivity.class);
                Log.v("Label","terminal");
                break;
            case R.id.setting:
                intent = new Intent(this, SettingActivity.class);
                break;
            case R.id.connexion:
                // on lance la connexion à l'esp
                if (!RoboTaf.tcpClient.isConnected()) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                // on se connecte à l'esp
                                RoboTaf.tcpClient.connect(RoboTaf.ipAddress, RoboTaf.port);

                                // on vérifie si on est bien connecté à l'esp
                                if (RoboTaf.tcpClient.isConnected()) {
                                    connexionOK.sendMessage(new Message());
                                } else {
                                    connexionKO.sendMessage(new Message());
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                connexionFailed.sendMessage(new Message());
                            }
                        }
                    }).start();
                }
                // si on était connecté, on ferme la connexion
                else {
                    RoboTaf.tcpClient.disconnect();
                    connexion.setImageResource(R.drawable.connexion);
                    Toast toast = Toast.makeText(StartActivity.this, R.string.disconnect, Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
        }
        if(intent!=null){
            startActivity(intent);}

    }
}
