package orleans.info.fr.robotaf.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import orleans.info.fr.robotaf.R;

/**
 * Created by Romain on 01/04/2017.
 */

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {
//public class SettingActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String PREFS = "PREFS";
    private static final String PREFS_NAME = "PREFS_NAME";
    private static final String PREFS_LANGUAGE = "PREFS_LANGUAGE";
    private static final String PREFS_THEME_PROG = "PREFS_THEME_PROG";
    SharedPreferences sharedPreferences;
    private Button langage, prog,valid;
    private TextView user;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_setting);
        sharedPreferences = getBaseContext().getSharedPreferences(PREFS, MODE_PRIVATE);

        user = (TextView) findViewById(R.id.user);
        langage = (Button) findViewById(R.id.language);
        prog = (Button) findViewById(R.id.prog);
        valid = (Button) findViewById(R.id.valid);
        langage.setOnClickListener(this);
        prog.setOnClickListener(this);
        valid.setOnClickListener(this);
        if (sharedPreferences.contains(PREFS_THEME_PROG)) {
            user.setText(sharedPreferences.getString(PREFS_NAME, "RoboUser"));
            if (sharedPreferences.getString("PREFS_LANGUAGE", "Français").equals("Français")) {
                langage.setText(R.string.Français);
            } else {
                langage.setText(R.string.Autre);
            }
            if (sharedPreferences.getString("PREFS_THEME_PROG", "Tableau Noir").equals("Tableau Noir")) {
                prog.setText(R.string.TableauNoir);
            } else {
                prog.setText(R.string.TableauBlanc);
            }
        } else {
            sharedPreferences
                    .edit()
                    .putString(PREFS_LANGUAGE, "Français")
                    .putString(PREFS_THEME_PROG, "Tableau Noir")
                    .putString(PREFS_NAME, "RoboUser")
                    .apply();
        }
    }
    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.prog:
                if (prog.getText().toString().equals("Tableau Noir")) {
                    prog.setText(R.string.TableauBlanc);
                } else {
                    prog.setText(R.string.TableauNoir);
                }
                break;
            case R.id.language:
                if (langage.getText().toString().equals(R.string.Français)) {
                    langage.setText(R.string.Français);
                } else {
                    langage.setText(R.string.Français);
                }
                break;
            case R.id.valid:
                sharedPreferences
                        .edit()
                        .putString(PREFS_LANGUAGE, langage.getText().toString())
                        .putString(PREFS_THEME_PROG, prog.getText().toString())
                        .putString(PREFS_NAME, user.getText().toString())
                        .apply();
                intent = new Intent(this, StartActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }
}