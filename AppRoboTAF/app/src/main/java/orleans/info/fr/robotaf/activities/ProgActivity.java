package orleans.info.fr.robotaf.activities;

import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import orleans.info.fr.robotaf.R;

/**
 * Created by Romain on 01/04/2017.
 */

public class ProgActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String PREFS = "PREFS";
    private static final String PREFS_THEME_PROG = "PREFS_THEME_PROG";
    private static final String DELIMITER_LOGO = "_";
    private static final String DELIMITER_FOR_LOGO = "!";
    private boolean color;
    //pour le moment
    private String sauvegarde;
    SharedPreferences sharedPreferences;
    /*
    ces trois compteur nous permettent de nous empécher de fermer une boucle anvant son ouverture
     */

    int cptfor;
    int cpttq;
    int cptif;


    //Ne sera pas utilisé pour le moment
    protected  boolean demande(ImageView iv){
        final String message;
        int random = (int) Math.floor(Math.random() * 2);
        boolean cani=false;
        switch (iv.getId()){
            case R.id.move:
                //on demande au robot si on peux avancer
                //le résultat (oui ou non) de cette demande est stocker dans cani

                Log.v("random","random"+random);
                if(random==1){
                    cani = true;
                }else{
                cani=false;//en attendant que la vérification soit possible
                }
                break;
            case R.id.left:
                //on demande au robot si on peux avancer
                //le résultat (oui ou non) de cette demande est stocker dans caniint random = (int) Math.floor(Math.random() * 2);
                Log.v("random","random"+random);
                if(random==1){
                    cani = true;
                }else{
                    cani=false;//en attendant que la vérification soit possible
                }
                break;
            case R.id.right:
                //on demande au robot si on peux avancer
                //le résultat (oui ou non) de cette demande est stocker dans caniint random = (int) Math.floor(Math.random() * 2);
                Log.v("random","random"+random);
                if(random==1){
                    cani = true;
                }else{
                    cani=false;//en attendant que la vérification soit possible
                }
                break;
            case R.id.back:
                //on demande au robot si on peux avancer
                //le résultat (oui ou non) de cette demande est stocker dans caniint random = (int) Math.floor(Math.random() * 2);
                Log.v("random","random"+random);
                if(random==1){
                    cani = true;
                }else{
                    cani=false;//en attendant que la vérification soit possible
                }
                break;
            case R.id.klaxoon:
                //on demande au robot si on peux avancer
                //le résultat (oui ou non) de cette demande est stocker dans caniint random = (int) Math.floor(Math.random() * 2);

                    cani = true;
            case R.id.led:
                //on demande au robot si on peux avancer
                //le résultat (oui ou non) de cette demande est stocker dans caniint random = (int) Math.floor(Math.random() * 2);

                cani = true;
            case R.id.stop:
                //on demande au robot si on peux avancer
                //le résultat (oui ou non) de cette demande est stocker dans caniint random = (int) Math.floor(Math.random() * 2);

                cani = true;
        }
        return cani;
    }


    protected int communication (LinearLayout ll,int cpt){
        //boolean autorisation = demande((ImageView) ll.getChildAt(cpt));
        Log.d("comm ->","cpt : " + cpt);
        switch (ll.getChildAt(cpt).getId()){
            case R.id.move:
                //if(autorisation) {


                //    RoboTaf.tcpClient.send("avancer()");
                Log.v("Communication","Move");

                /*}
                while(RoboTaf.tcpClient.recv()!="ok" && RoboTaf.tcpClient.recv()!="ko")
                {
                }
                if(RoboTaf.tcpClient.recv()=="ok"){
                    ll.getChildAt(cpt).setBackgroundDrawable(getResources().getDrawable(R.drawable.shapeok));
                }else if(RoboTaf.tcpClient.recv()=="ko"){
                    ll.getChildAt(cpt).setBackgroundDrawable(getResources().getDrawable(R.drawable.shapeko));
                    return -1;
                }*/
                return cpt+1;
            case R.id.left:
                //RoboTaf.tcpClient.send("gauche()");
                Log.v("Communication","Left");
                return cpt+1;
            case R.id.right:
                //RoboTaf.tcpClient.send("droite()");
                Log.v("Communication","Right");
                return cpt+1;
            case R.id.back:
                //return cpt+1;
                //autorisation = demande((ImageView) ll.getChildAt(cpt));
                //if(autorisation){
                    //envoie du message au robot
                    //Dès qu'on recoi ok on return


                Log.v("Communication","Back");
                //RoboTaf.tcpClient.send("reculer()");
                 return cpt+1;
                //}else return -1;
            case R.id.stop:
                //RoboTaf.tcpClient.send("gauche()");
                Log.v("Communication","stop");
                return cpt+1;
            case R.id.led:

                Log.v("Communication","Led");
//                RoboTaf.tcpClient.send("connectedLed()");
                return cpt+1;
            case R.id.klaxoon:

                Log.v("Communication","Klaxon");
               // RoboTaf.tcpClient.send("klaxon()");
                return cpt+1;
            case R.id.forend://pas besoin normalement

                Log.v("Communication","Forend");
                return cpt+1;
            case R.id.tqend://pas besoin normalement

                Log.v("Communication","tqend");
                return cpt+1;
            case R.id.sinon://pas besoin normalement

                Log.v("Communication","Sinon");
                return cpt+1;
            case R.id.siend://pas besoin normalement

                Log.v("Communication","Siend");
                return cpt+1;

            case R.id.fore:
            case -1:
                Log.v("Communication","For");
                LinearLayout lltmp = (LinearLayout) ll.getChildAt(cpt);
                EditText edit = (EditText) lltmp.getChildAt(1);
                int i = Integer.parseInt(edit.getText().toString());
                int mem =cpt;
                while(i!=0) {

                    while(ll.getChildAt(cpt+1)==null || ll.getChildAt(cpt+1).getId()!=R.id.forend){
                        Log.v("fore","child " + (cpt+1) + " : " + (((ll.getChildAt(cpt+1)==null) ? "null" : ((ll.getChildAt(cpt+1).getId()!=R.id.forend) ? "!forend"  : ""))));
                        cpt++;
                        communication(ll, cpt);
                    }
                    i--;
                    Log.v("TEST","ll.getChildAt(cpt+1).getId()==R.id.forend --> " + (ll.getChildAt(cpt+1).getId()==R.id.forend) + " i!=0 --> " +i);
                    if(ll.getChildAt(cpt+1).getId()==R.id.forend && i!=0){
                        cpt=mem;
                    }//else
                        //return "erreur"
                }
                Log.v("TEST","ll.getChildAt(cpt+1).getId()==R.id.forend --> " + (ll.getChildAt(cpt+1).getId()==R.id.forend));
                if(ll.getChildAt(cpt+1).getId()==R.id.forend){
                    return cpt+2;//cpt+1 (saute le forend??
                }
                break;
            case R.id.tq:
                //fonction demande
                //ex, puis je tourner??

                Log.v("Communication","Tq -> " + cpt);
                mem =cpt;
                if(ll.getChildAt(cpt+2).getId()==R.id.tqend) return -2;
                boolean repeat = demande((ImageView) ll.getChildAt(mem+1));//On cherche à voir si on la condition est satisfiable, Je ne sais pas quelle
                    //fonction doit être appelé, ceci sera à mettre dans demande.
                int pitTq = 0;
                cpt += 2;
                Log.v("tq bool", " -> " + repeat);
                if(repeat) {
                    Log.d("pre while", " ? " + (ll.getChildAt(cpt).getId() != R.id.tqend));
                    while (ll.getChildAt(cpt).getId() != R.id.tqend) {
                        Log.d("while", " cpt ok et +1 ? " + (ll.getChildAt(cpt + 1).getId() != R.id.tqend) + "+2 ? " + (ll.getChildAt(cpt + 2).getId() != R.id.tqend));
                        if (pitTq == 0 && ll.getChildAt(cpt).getId() != R.id.tqend) {
                            communication(ll, cpt);
                        }
                        cpt++;
                        if (ll.getChildAt(cpt).getId() == R.id.tq) {
                            Log.d("Tq desous", "->");
                            communication(ll, cpt);
                            Log.d("Tq emmerge", "<-");
                            pitTq++;
                        } else if (pitTq > 0 && ll.getChildAt(cpt).getId() == R.id.tqend) {
                            pitTq--;
                            cpt++;
                        } else if (pitTq == 0 && ll.getChildAt(cpt).getId() == R.id.tqend) {
                            Log.d("IBreakIt", "bouya");
                            break;
                        } else Log.d("fuck", "");

                    }
                    Log.d("dangerZoneTrue", "aaaaaaaaaahhhhhhhhhhhhhh");
                    return mem;
                }
                else{
                    while (ll.getChildAt(cpt).getId() != R.id.tqend) {
                        Log.d("while", " cpt ok et +1 ? " + (ll.getChildAt(cpt + 1).getId() != R.id.tqend) + "+2 ? " + (ll.getChildAt(cpt + 2).getId() != R.id.tqend));
                        cpt++;
                        if(ll.getChildAt(cpt).getId() == R.id.tq) {
                            pitTq++;
                        } else if(pitTq > 0 && ll.getChildAt(cpt).getId() == R.id.tqend) {
                            pitTq--;
                            cpt++;
                        } else if(pitTq == 0 && ll.getChildAt(cpt).getId() == R.id.tqend) {
                            Log.d("IBreakItFalse", "bouya");
                            break;
                        } else Log.d("fuck", "");

                    }
                    Log.d("dangerZoneFalse", "pouf");
                    return cpt+1;
                }
            case R.id.si:

                Log.v("Communication","Si");
                if(demande((ImageView) ll.getChildAt(cpt+1))){//à gérer

                    Log.v("Si","True");
                    mem=cpt;
                    cpt=cpt+2;
                    while (ll.getChildAt(cpt).getId()!=R.id.siend && ll.getChildAt(cpt).getId()!=R.id.sinon){
                        communication(ll, cpt);
                        cpt++;
                    }
                    while(ll.getChildAt(cpt).getId()!=R.id.siend){
                        cpt++;
                    }
                    return cpt;//
                }else{

                    Log.v("SI","False");
                    while (ll.getChildAt(cpt).getId()!=R.id.siend && ll.getChildAt(cpt).getId()!=R.id.sinon){
                        Log.v("while si false","ll enfant : " + cpt + " : " + ll.getChildAt(cpt).getId() + ", siend : " + R.id.siend + ", siend : " + R.id.sinon);
                        cpt++;
                    }
                    return cpt;//return cpt+1;//
                }


        }
        Log.v("RETURN","ll.getChildAt(cpt).getId()"+(ll.getChildAt(cpt).getId()));
        return -1;
    }
    @Override
    protected void onCreate(Bundle savedInstanceSatate) {
        super.onCreate(savedInstanceSatate);
        setContentView(R.layout.activity_prog);
        sharedPreferences = getBaseContext().getSharedPreferences(PREFS, MODE_PRIVATE);


        findViewById(R.id.move).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.left).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.right).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.back).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.stop).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.led).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.song).setOnTouchListener(new MyTouchListener());

        findViewById(R.id.si).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.sinon).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.siend).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.fore).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.forend).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.tq).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.tqend).setOnTouchListener(new MyTouchListener());
        //findViewById(R.id.et).setOnTouchListener(new MyTouchListener());
        //findViewById(R.id.ou).setOnTouchListener(new MyTouchListener());
        //findViewById(R.id.non).setOnTouchListener(new MyTouchListener());


        findViewById(R.id.newpage).setOnClickListener(this);

        findViewById(R.id.dropzone).setOnDragListener(new MyDragListener());
        findViewById(R.id.dropzone2).setOnDragListener(new MyDragListener());

        findViewById(R.id.validate).setOnClickListener(this);
        findViewById(R.id.infomove).setOnClickListener(this);
        findViewById(R.id.infoleft).setOnClickListener(this);
        findViewById(R.id.inforight).setOnClickListener(this);
        findViewById(R.id.infoback).setOnClickListener(this);
        findViewById(R.id.infostop).setOnClickListener(this);
        findViewById(R.id.infoled).setOnClickListener(this);
        findViewById(R.id.infosong).setOnClickListener(this);

        findViewById(R.id.infosi).setOnClickListener(this);
        findViewById(R.id.infosiend).setOnClickListener(this);
        findViewById(R.id.infosinon).setOnClickListener(this);
        findViewById(R.id.infofore).setOnClickListener(this);
        findViewById(R.id.infoforend).setOnClickListener(this);
        findViewById(R.id.infotq).setOnClickListener(this);
        findViewById(R.id.infotqend).setOnClickListener(this);
        findViewById(R.id.infoet).setOnClickListener(this);
        findViewById(R.id.infoou).setOnClickListener(this);
        findViewById(R.id.infonon).setOnClickListener(this);

        findViewById(R.id.save).setOnClickListener(this);
        findViewById(R.id.load).setOnClickListener(this);
        cptfor = 1;
        cpttq = 1;
        cptif = 1;
        color = false;
        ImageView corbeille = (ImageView) findViewById(R.id.dropzone2);
        LinearLayout board = (LinearLayout) findViewById(R.id.board);
        ImageButton save = (ImageButton) findViewById(R.id.save);
        ImageButton load = (ImageButton) findViewById(R.id.load);
        ImageButton delete = (ImageButton) findViewById(R.id.delete);

        save.setOnClickListener(this);
        load.setOnClickListener(this);
        delete.setOnClickListener(this);
        if(sharedPreferences.getString(PREFS_THEME_PROG,"Tableau Noir").equals("Tableau Blanc")){
            board.setBackgroundResource(R.drawable.boardwhite);
            corbeille.setImageResource(R.drawable.corbeilleblack);
            save.setImageResource(R.drawable.saveblack);
            load.setImageResource(R.drawable.loadblack);
            delete.setImageResource(R.drawable.deleteblack);
            /*findViewById(R.id.sv1).setBackgroundDrawable(getResources().getDrawable(R.drawable.shapeblack));
            findViewById(R.id.sv2).setBackgroundDrawable(getResources().getDrawable(R.drawable.shapeblack));
            findViewById(R.id.sv3).setBackgroundDrawable(getResources().getDrawable(R.drawable.shapeblack));*/
        }

    }

    private final class MyTouchListener implements OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0);
                view.setVisibility(View.VISIBLE);
                return true;
            } else {
                return false;
            }
        }
    }

    private boolean colorOrNot(boolean color, boolean elem){
        if(color){
            color = false;
        }
        if(elem){
            color=true;
        }
        return color;

    }

    private class MyDragListener implements OnDragListener {
        Drawable enterShape = getResources().getDrawable(R.drawable.shape_droptarget);
        Drawable normalShape = getResources().getDrawable(R.drawable.shape);
        boolean elem=false;

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundDrawable(enterShape);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundDrawable(normalShape);
                    break;
                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    /*
                    Si on largue le logo dans la zone centrale et que notre loge vient d'une de deux cotés et non du centre
                     */
                    if((v.getId()==R.id.dropzone) && (view.getTag()==findViewById(R.id.validate).getTag())){
                        elem = addLogo(v,view,elem);
                    }
                    /*
                    Si on largue le logo dans la zone corbeille
                     */
                    if(v.getId()==R.id.dropzone2 && (view.getTag()!=findViewById(R.id.validate).getTag())){
                        if(isNeedCond(view.getId())) {
                            Log.d("suppr", "id : " + view.getId());
                            removeLogo(view, owner);
                        }
                        else{
                            //on recherche son grand frère
                            int i=0;
                            if(owner.getChildAt(i)!=view){
                                while(owner.getChildAt(i+1)!=view){
                                    i++;
                                }
                            }
                            //i est le grand frère
                            Log.d("suppr","pereid : " + owner.getChildAt(i).getId());
                            if(isNeedCond(owner.getChildAt(i).getId())) {
                                Log.d("suppr", "pere statement");
                                removeLogo(owner.getChildAt(i),owner);
                            }
                            else{
                                Log.d("suppr","pere non statement");
                                removeLogo(view, owner);
                            }
                        }
                        break;
                    }
                    view.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setBackgroundDrawable(normalShape);
                    Log.v("CPT FIN", "for-->" + cptfor+"   tq-->"+cpttq);
                default:
                    break;
            }
            return true;
        }

        private boolean isNeedCond(int id){
            Log.d("isNeedCond","id : " + id + ", tq : " + R.id.tq + ", si : " + R.id.si);
            switch(id){
                case R.id.tq:
                case R.id.si:
                    return true;
            }
            Log.d("isNeedCond","false");
            return false;
        }

        private void removeLogo(View view, ViewGroup owner){
            switch (view.getId()){
                //Ici on supp les images que l'on souhaite retirer.
                case R.id.sinon:
                    int i=0;
                    //on récupère la position de notre élément dans la vue
                    while (owner.getChildAt(i)!=view){
                        i++;
                    }
                    i++;
                    //On regarde si après notre logo un logo existe, s'ilo n'existe pas on arrète tout
                    if ((owner.getChildAt(i)==null)){
                        break;
                    }//On regarde si après notre logo un logo existe
                    if(owner.getChildAt(i).getId()==R.id.si){
                        if(owner.getChildAt(i+1)!=null){
                            //si le logo d'après est un si alors on supprimme la condition qu'il y a après le si
                        owner.removeView((((ViewGroup) view.getParent()).getChildAt(i+1)));
                        }
                        //On retir ele logo if
                        owner.removeView((((ViewGroup) view.getParent()).getChildAt(i)));
                    }
                    break;
                case R.id.si:
                    boolean b=false;
                    i=0;
                    //on récupère la position de notre élément dans la vue
                    while (owner.getChildAt(i)!=view){
                        i++;
                    }
                    i++;
                    ArrayList <Integer> mem = new ArrayList<Integer>();
                    //On ajoute notre position dans une arraylist, cette arraylist nous permet de supprimmer tous les logos associés à la suppression du if
                    mem.add(i);
                    int tmp =0;
                    boolean first =true;
                    while(!b){
                        if((owner.getChildAt(i)==null)||(owner.getChildAt(i).getId()==R.id.siend && tmp==0)) {
                            break;
                        }
                        //On regarde si devant notre logo si il y a un logo sinon
                        if(i-2>=0 && first) {
                            //Si il y a un logo sinon, et que notre compteur est nul alors on supprimme le tout (Cette vérification est fait qu'une seule fois
                            if(owner.getChildAt(i-2).getId()==R.id.sinon && tmp==0){
                                mem.remove(0);//On retire le premier élément afin de garder un ordre correct pour ne pas avoir de problème lors de la suppression
                                mem.add(i-2);
                                mem.add(i);
                                break;
                            }else
                                first=false;
                        }else if(owner.getChildAt(i).getId()==R.id.siend){
                            tmp--; //On décrément le compteur car tmp est différent de zéro donc ce siend n'est pas le siend associé à notre si
                        }else if(owner.getChildAt(i).getId() == R.id.si && owner.getChildAt(i-1).getId() == R.id.sinon){
                            if(tmp==0){//Ici on ajoute juste le else afin de le supprimer en même temps que notre si.
                                mem.add(i-1);
                                break;
                            }
                        } else if (owner.getChildAt(i).getId() == R.id.si) {
                            tmp++;//Si on tombe sur un Si mais que l'élément i-1 n'est pas un sinon alors on augmente notre compteur
                        } else if (owner.getChildAt(i-1).getId() == R.id.sinon){
                            if(tmp==0){// si on est sur un else qui est en lien avec le if que l'on veut supprimmer
                                if(!mem.contains(i-1)){
                                    mem.add(i-1);//Si il n'est pas déjà présent on le rajoute
                                }
                            }
                        }
                        i++;
                        b=owner.getChildAt(i)==null;
                    }
                    if(owner.getChildAt(i)!=null && owner.getChildAt(i).getId()==R.id.siend){
                        //On supprimme le siend si il est à supprimmer
                        owner.removeView((((ViewGroup) view.getParent()).getChildAt(i)));
                        cptif++;//on augmente le compteur  car suppression du siend
                    }
                    if (!mem.isEmpty()){
                        //on supprime tout les élément stocké dans notre tableau
                        for(int t = mem.size()-1;t>=0;t--){
                            owner.removeView((((ViewGroup) view.getParent()).getChildAt(mem.get(t))));
                        }
                    }
                    cptif--;//on diminu le compteur  car suppression du si
                    break;

                case R.id.tq:
                    b=false;
                    i=0;
                    //on récupère la position de notre élément dans la vue
                    while (owner.getChildAt(i)!=view){
                        i++;
                    }
                    int j=i;
                    i++;
                    tmp =0;
                    while(!b){
                        if((owner.getChildAt(i)==null)||(owner.getChildAt(i).getId()==R.id.tqend && tmp==0)) {
                            break;//Si notre élément est le tqend avec un tmp de zéro alors on à l'élément qu'on cherche à supprimer
                        }if(owner.getChildAt(i).getId()==R.id.tqend){
                            tmp--;//si le compteur n'est pas 0 alors on décrémente le compteur
                        }else if(owner.getChildAt(i).getId()==R.id.tq){
                            tmp++;//si le compteur n'est pas 0 alors on incrémente le compteur
                        }
                        i++;
                        b=owner.getChildAt(i)==null;
                    }
                    if(owner.getChildAt(i)!=null){
                        //On supprimme le tqend si il est à supprimmer
                        owner.removeView((((ViewGroup) view.getParent()).getChildAt(i)));
                        cpttq++;
                    }
                    if(owner.getChildAt(j+1)!=null){
                        //On supprimme la condition suivant le tq s'il existe
                        owner.removeView((((ViewGroup) view.getParent()).getChildAt(j+1)));
                    }
                    cpttq--;//on supprime un logo tq donc on met à jour notre compteur
                    break;
                case R.id.fore:
                    b=false;
                    i=0;
                    ViewGroup principal = (ViewGroup) owner.getParent();
                    //Ici c'est différent des autre logo car nous n'avons pas une imageView mais un lineair layout. Le logo for est associé à un édit text
                    //Mais c'est le même principe, on cherche sa position
                    while (owner.getChildAt(i)!=null ||principal.getChildAt(i)!=null) {
                        //on vérifie si notre élément est un ImageView
                        if (principal.getChildAt(i).getClass() != findViewById(R.id.move).getClass()) {
                            ViewGroup secondaire = (ViewGroup) principal.getChildAt(i);
                            if (owner == secondaire) {
                                break;
                            }
                        }
                        i++;
                    }
                    i++;
                    tmp =0;
                    while(!b){
                        //Si l'élément n'exisite pas ou si c'est nouqs somme sur la fermeture associé à notre logo
                        if((principal.getChildAt(i)==null)||(principal.getChildAt(i).getId()==R.id.forend && tmp==0)) {
                            break;
                        }
                        if(principal.getChildAt(i).getId()==R.id.forend){
                            tmp--;//si le compteur n'est pas 0 alors on décrémente le compteur
                        }else if(principal.getChildAt(i).getClass()==owner.getClass()){
                            tmp++;//si le compteur n'est pas 0 alors on incrémente le compteur
                        }
                        i++;
                        b=principal.getChildAt(i)==null;
                    }
                    if(principal.getChildAt(i)!=null){
                        //on supprim le forend s'il existe
                        principal.removeView(((principal.getChildAt(i))));
                        cptfor++; //donc on incrémente le compteur de différence For/Forend
                    }
                    owner.removeView((((ViewGroup) view.getParent()).getChildAt(1)));
                    //On supprime l'edit text associé au for
                    cptfor--;// on décrément les compteur car on va supprimmer notre for
                    break;
                case R.id.tqend:
                    cpttq++; // Si on supprimme un logo tqend un incrémente le compteur de différence tq/tqend
                    break;
                case R.id.forend:
                    cptfor++;// Si on supprimme un logo tqend un incrémente le compteur de différence for/forend
                    break;
                case R.id.siend:
                    cptif++;// Si on supprimme un logo tqend un incrémente le compteur de différence si/siend
                    break;
            }
            int lab=0;
            //Ici on récupère la position de notre élément
            while(owner.getChildAt(lab)!=view){
                lab++;
            }
            //On verifie si c'est le dernier élément, et si il faut retirer le blocage de certain logo
            if(owner.getChildAt(lab+1)==null){
                color=false;
                elem=false;
            }
            //Ici on supprimme notre logo
            owner.removeView(view);
            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        //On initialise notre boite de dialogue
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        AlertDialog.Builder alertDialogBuilderConfirm = new AlertDialog.Builder(this);
        AlertDialog.Builder alertDialogBuilderEmpty = new AlertDialog.Builder(this);

        AlertDialog alertDialog;
        switch (v.getId()){
            case R.id.delete:
                //final AutoCompleteTextView myAutoComplete = new AutoCompleteTextView(ProgActivity.this);
                // autocomplete thingie
                final ArrayList<String> mSelectedItems = new ArrayList<>();
                String keys2 = recupKeySharedPreference();
                Log.d("keys", "k : " + keys2);
                if(keys2!=null && keys2.equals("")){
                    alertDialogBuilder.setTitle("Suppression");
                    alertDialogBuilder
                            .setCancelable(false)
                            .setMessage("Aucun programme à supprimer")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
                }else {
                    final CharSequence[] keyTab2 = keys2.split(DELIMITER_LOGO);
                    for (int i = 0; i < keyTab2.length; i++) {
                        Log.d("k2", "i : " + i + ", k2i : " + keyTab2[i]);
                    }
                    alertDialogBuilder.setTitle("Suppression");
                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setMultiChoiceItems(keyTab2, null, new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                    // TODO Auto-generated method stub
                                    if (isChecked) {
                                        // If the user checked the item, add it to the selected items
                                        //mSelectedItems2.add(keyTab2[which].toString(),which);
                                        mSelectedItems.add(keyTab2[which].toString());
                                    } else if (mSelectedItems.contains(keyTab2[which].toString())) {
                                        // Else, if the item is already in the array, remove it
                                        mSelectedItems.remove(keyTab2[which].toString());
                                    }
                                }
                            })
                            .setNegativeButton("NON", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //What ever you want to do with the value
                                    removeKey(mSelectedItems);
                                    dialog.cancel();
                                }
                            });
                }
                break;
            case R.id.save:
                final EditText edittext = new EditText(ProgActivity.this);
                alertDialogBuilderConfirm.setTitle("Deja existant");
                // set dialog message
                alertDialogBuilderConfirm
                    .setMessage("Ecraser le programme existant ?")
                    .setCancelable(false)
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //What ever you want to do with the value
                            addSharedPreference(edittext.getText().toString().toLowerCase(),save((ViewGroup) findViewById(R.id.dropzone)));
                            dialog.cancel();
                        }
                    });
                final AlertDialog alertDialogConfirm = alertDialogBuilderConfirm.create();

                alertDialogBuilderEmpty.setTitle("Nom vide");
                // set dialog message
                alertDialogBuilderEmpty
                        .setMessage("Veuilliez mettre au moins un caractère")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                //What ever you want to do with the value
                                addSharedPreference(edittext.getText().toString().toLowerCase(),save((ViewGroup) findViewById(R.id.dropzone)));
                                dialog.cancel();
                            }
                        });
                final AlertDialog alertDialogEmpty = alertDialogBuilderEmpty.create();

                if(((ViewGroup)findViewById(R.id.dropzone)).getChildAt(0)!=null) {
                    alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setTitle("Sauvegarder");
                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Nom du programme ?")
                            .setView(edittext)
                            .setCancelable(false)
                            .setNegativeButton("NON", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //What ever you want to do with the value
                                    String key = edittext.getText().toString().toLowerCase();
                                    if (key.length() == 0){
                                        alertDialogEmpty.show();
                                    }else if(sharedPreferences.contains(key)) {
                                        alertDialogConfirm.show();
                                    }else{
                                        addSharedPreference(key, save((ViewGroup) findViewById(R.id.dropzone)));
                                    }
                                }
                            });
                }else{
                    alertDialogBuilder.setTitle("Sauvegarder");
                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Programme vide")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //What ever you want to do with the value
                                    dialog.cancel();
                                }
                            });
                }
                break;
            case R.id.load:

                //final AutoCompleteTextView myAutoComplete = new AutoCompleteTextView(ProgActivity.this);
                // autocomplete thingie
                String keys = recupKeySharedPreference();
                if(keys!=null && keys.equals("")){
                    alertDialogBuilder.setTitle("Charger");
                    alertDialogBuilder
                            .setCancelable(false)
                            .setMessage("Aucun programme sauvegarder")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
                }else {
                    Log.d("keys", "k : " + keys);
                    final CharSequence[] keyTab = keys.split("_");
                    for (int i = 0; i < keyTab.length; i++) {
                        Log.d("ll", "i : " + i + ", ki : " + keyTab[i]);
                    }
                    alertDialogBuilder.setTitle("Charger");
                    alertDialogBuilder
                            .setCancelable(false)
                            .setSingleChoiceItems(keyTab, -1, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                    load((ViewGroup)findViewById(R.id.dropzone), keyTab[which].toString());
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton("NON", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                }
                break;
            case R.id.newpage:
                if(((ViewGroup)findViewById(R.id.dropzone)).getChildAt(0)!=null) {
                    alertDialogBuilder.setTitle("Tout supprimer");
                    alertDialogBuilder
                            .setMessage("Es tu sûr de vouloir tout supprimer")
                            .setCancelable(false)
                            .setNegativeButton("NON", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    ProgActivity.this.recreate();
                                    dialog.cancel();

                                }

                            });
                }
                else{
                    alertDialogBuilder.setTitle("Tout supprimer");
                    alertDialogBuilder
                            .setMessage("La zone est déjà vide")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                }
                break;

            case R.id.validate:
                /*
                *Permet de récupérer les élément central
                 */
                final LinearLayout container = (LinearLayout) findViewById(R.id.dropzone);
                int i=0;

                alertDialogBuilderConfirm.setTitle("Boucle non fermé");
                // set dialog message
                alertDialogBuilderConfirm
                        .setMessage("Tes boucles sont elles bien fermées ?")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                //What ever you want to do with the value
                                dialog.cancel();
                            }
                        });
                final AlertDialog alertDialogBoucle = alertDialogBuilderConfirm.create();



                /*while(container.getChildAt(i)!=null){
                    Log.d("pre com","i : " + i);
                        i=communication(container, i);
                    Log.d("post com", "i : " + i);*/

                        /*LinearLayout container2 = (LinearLayout) container.getChildAt(i);
                        container2.getChildAt(0);
                        EditText chaine = (EditText) container2.getChildAt(1);
                        String text = chaine.getText().toString();
                        long val = Long.valueOf(text);
                    }else{
                        container.getChildAt(i);*/
                //}

                // on met un titre
                alertDialogBuilder.setTitle(R.string.TitleSend);

                // on met notre message
                alertDialogBuilder
                        .setMessage(R.string.MsgSend)
                        .setCancelable(false)
                        .setNegativeButton(R.string.Neg ,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }

                        })
                        .setPositiveButton(R.string.Pos,new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // on ferme juste la boite de dialogue si on clic
                                        int i = 0;
                                        if(cpttq==1 && cptfor==1 && cptif==1) {
                                            while (container.getChildAt(i) != null) {
                                                Log.d("pre com", "i : " + i);
                                                i = communication(container, i);
                                                Log.d("post com", "i : " + i);
                                            }
                                        }
                                        else{
                                            alertDialogBoucle.show();
                                        }
                                        dialog.cancel();
                                    }
                        });
                break;
                case R.id.infomove:
                    // set title
                    alertDialogBuilder.setTitle(R.string.TitleMove);

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(R.string.MsgMove)
                            .setCancelable(false)
                            .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
                    break;
            case R.id.infoleft:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleLeft);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgLeft)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.inforight:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleRight);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgRight)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infoback:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleBack);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgBack)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infostop:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleStop);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgStop)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infoled:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleLed);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgLed)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infosong:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleKlaxoon);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgKlaxoon)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infofore:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleFor);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgFor)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infoforend:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleForend);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgForend)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infotq:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleTq);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgTq)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infotqend:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleTqend);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgTqend)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infosi:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleIf);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgIf)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infosinon:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleElse);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgElse)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infosiend:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleSend);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgIfend)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infoet:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleEt);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgEt)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infoou:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleOu);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgOu)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
            case R.id.infonon:
                // set title
                alertDialogBuilder.setTitle(R.string.TitleNon);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.MsgNon)
                        .setCancelable(false)
                        .setPositiveButton(R.string.Ok,new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                break;
        }
        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        if(intent!=null){
            startActivity(intent);}
    }


    private String recupKeySharedPreference() {
        if (sharedPreferences.getAll() != null) {
            Map<String, String> msp = new HashMap<String, String>();
            msp = (Map<String, String>) sharedPreferences.getAll();
            Set<Map.Entry<String, String>> setMsp = msp.entrySet();
            Iterator<Map.Entry<String, String>> it = setMsp.iterator();
            String s = "";
            while (it.hasNext()) {
                Map.Entry<String, String> e = it.next();
                String test = e.getKey();
                if(test.length() < 5 || !test.substring(0,5).equals("PREFS")) {
                    s += e.getKey() + "_";
                    System.out.println(e.getKey() + " : " + e.getValue() + ", s : " + s);
                }
                else{
                    System.out.println(e.getKey() + " PREF!!!!!!");
                }
            }
            return s;
        }
        return "";
    }


    private void addSharedPreference(String key, String value){
        sharedPreferences
                .edit()
                .putString(key, value)
                .apply();
        showSharedPreference();
    }

    private void removeKey(ArrayList<String> select) {
        for(String remove : select){
            Log.d("remove ->","remove : " + remove);
            showSharedPreference();
            sharedPreferences
                    .edit()
                    .remove(remove)
                    .apply();
            Log.d("remove <-","");
            showSharedPreference();
        }
    }

    private void showSharedPreference() {
        Log.d("showSharedPreference ->","");
        if(sharedPreferences.getAll()!=null) {
            Map<String, ?> msp = sharedPreferences.getAll();
            Set<? extends Map.Entry<String, ?>> setMsp = msp.entrySet();
            for(Map.Entry<String, ?> e : setMsp) {
                System.out.println(e.getKey() + " : " + e.getValue());
            }
        }
        else{
            Log.d("ssp","null");
        }
    }

    private String save(ViewGroup owner){
        String sauv = "";
        int i = 0;
        while (owner.getChildAt(i)!=null){
            String enfant = String.valueOf(owner.getChildAt(i).getId());
            if(enfant.equals("-1")) {
                LinearLayout fore = (LinearLayout) owner.getChildAt(i);
                sauv += DELIMITER_FOR_LOGO + fore.getChildAt(0).getId() + DELIMITER_FOR_LOGO + ((EditText)fore.getChildAt(1)).getText().toString() + DELIMITER_LOGO;
            }else {
                sauv += String.valueOf(owner.getChildAt(i).getId()) + DELIMITER_LOGO;
            }
            Log.d("sauvegarde","owner i : " + owner.getChildAt(i).getId() + ", sauv : " + sauv);
            i++;
        }
        return sauv;
    }

    private void load(ViewGroup owner, String key){
        Map<String, String> msp = (Map<String, String>) sharedPreferences.getAll();
        String sauv = msp.get(key);
        Log.d("charger","sauv : " + sauv);
        String[] charge = sauv.split(DELIMITER_LOGO);
        owner.removeAllViews();
        for (int i = 0; i < charge.length; i++) {
            Log.d("charger","s : " + charge[i]);
            if(charge[i].charAt(0)=='!'){
                String[] chargeFor = charge[i].split(DELIMITER_FOR_LOGO);
                Log.d("charger","cf0 : " + chargeFor[0] + ", cf1 : " + chargeFor[1] + ", cf2 : " + chargeFor[2]);
                addLogo(owner,findViewById(Integer.parseInt(chargeFor[1])),false);
                ((EditText)((LinearLayout)owner.getChildAt(i)).getChildAt(1)).setText(chargeFor[2]);
            }
            else{
                addLogo(owner,findViewById(Integer.parseInt(charge[i])),false);
            }
        }
    }

    private boolean addLogo(View v, View view, boolean elem){
        boolean bloquer=false;
        LinearLayout container = (LinearLayout) v;
        View viewclone=null;
        switch (view.getId()){
            //Ici on drop les images que l'on souhaite mettre. Certaines on des condition pour éviter tout problème
            case R.id.move:
                //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.move, null);
                break;
            case R.id.right:
                //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.right, null);
                break;
            case R.id.left:
                //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.left, null);
                break;
            case R.id.back:
                //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.back, null);
                break;
            case R.id.stop:
                //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.stop, null);
                break;
            case R.id.song:
                //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.song, null);
                break;
            case R.id.led:
                //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.led, null);
                break;
            case R.id.fore:
                if(!color){//Vérifiction si on attend une conditionnelle ou non
                    //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                    viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.fore, null);
                    cptfor++;//On augmente le compteur puisqu'on ouvre une boucle for
                }
                else
                    bloquer=true;//on attend une conditionnelle donc on bloque le drop
                break;
            case R.id.forend:
                if(!color){//Vérifiction si on attend une conditionnelle ou non
                    if(cptfor>0) {//Vérifiction si il y a au moins une ouverture de boucle supplémentaire
                        //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                        viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.forend, null);
                        cptfor--; //On diminue le compteur puisqu'on ferme une boucle while
                    }
                }else
                    bloquer=true; //on attend une conditionnelle donc on bloque le drop
                break;
            case R.id.tq:
                if(!color){//Vérifiction si on attend une conditionnelle ou non
                    //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                    viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.tq, null);
                    cpttq++;//On augmente le compteur puisqu'on ouvre une boucle while
                    elem=true;//On renseinge le fait que l'on attend un élément que l'on mettra comme condition
                }
                else
                    bloquer=true;//on attend une conditionnelle donc on bloque le drop
                break;
            case R.id.tqend:
                if(!color){//Vérifiction si on attend une conditionnelle ou non
                    if(cpttq>0) {
                        //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                        viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.tqend, null);
                        cpttq--;//On diminu le compteur puisqu'on ouvre une boucle while
                    }
                }
                else
                    bloquer=true;//on attend une conditionnelle donc on bloque le drop
                break;
            case R.id.si:
                if(!color){//Vérifiction si on attend une conditionnelle ou non
                    //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                    viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.si, null);
                    //On récupère le nompbre d'élément dropé depuis le début
                    int tmpc = container.getChildCount();
                    //Si le dernier élément placé est différent d'un logo Sinon
                    if(container.getChildAt(tmpc-1)==null || container.getChildAt(tmpc-1).getId()!=R.id.sinon){
                        // si il est différent on augmente le compteur, sinon on ne l'augement pas.
                        cptif++;
                    }
                    elem=true;}//On renseinge le fait que l'on attend un élément que l'on mettra comme condition
                else
                    bloquer=true;//on attend une conditionnelle donc on bloque le drop
                break;
            case R.id.sinon:
                if(!color){//Vérifiction si on attend une conditionnelle ou non
                    //On récupère le nompbre d'élément dropé depuis le début
                    int tmpc=container.getChildCount();
                    //On regarde les éléments, du plus récent au plus vieux
                    while(tmpc>0){
                        if(container.getChildAt(tmpc-1).getId()==R.id.si){
                            //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                            viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.sinon, null);
                            bloquer=true;//on attend une conditionnelle donc on bloque le drop
                            break;
                        }else if (container.getChildAt(tmpc-1).getId()==R.id.sinon || container.getChildAt(tmpc-1).getId()==R.id.siend){
                            // On ne fait rien
                            break;
                        }
                        tmpc--;
                    }
                }else
                    bloquer=true;//on attend une conditionnelle donc on bloque le drop
                break;
            case R.id.siend:
                if(!color){//Vérifiction si on attend une conditionnelle ou non
                    if(cptif>1) {
                        //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                        viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.siend, null);
                        cptif--;//On diminu le compteur puisqu'on ferme un if
                    }
                }else
                    bloquer=true;//on attend une conditionnelle donc on bloque le drop
                break;


            case R.id.et:
                if(container.getChildAt(0)!=null){
                    if(!color){//Vérifiction si on attend une conditionnelle ou non
                        //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                        viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.et, null);
                        elem=true;}//On renseinge le fait que l'on attend un élément que l'on mettra comme condition
                    else
                        bloquer=true;//on attend une conditionnelle donc on bloque le drop
                }
                break;
            case R.id.ou:
                if(container.getChildAt(0)!=null) {
                    if (!color) {//Vérifiction si on attend une conditionnelle ou non
                        //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                        viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.ou, null);
                        elem = true;//On renseinge le fait que l'on attend un élément que l'on mettra comme condition
                    } else
                        bloquer = true;//on attend une conditionnelle donc on bloque le drop
                }
                break;
            case R.id.non:
                if(container.getChildAt(0)!=null) {
                    if (!color) {//Vérifiction si on attend une conditionnelle ou non
                        //On clone l'item que l'on drop afin de le déposer au centre et afin qu'il ne disparaisse pas de la ou on le récupère
                        viewclone = LayoutInflater.from(view.getContext()).inflate(R.layout.non, null);
                        elem = true;//On renseinge le fait que l'on attend un élément que l'on mettra comme condition
                    } else
                        bloquer = true;//on attend une conditionnelle donc on bloque le drop
                }
                break;
        }
        if(view.getId()==R.id.forend && cptfor<=0){
            cptfor++;
        }else if(view.getId()==R.id.tqend && cpttq==0) {
            cpttq++;
        }else if(viewclone!=null&&(!bloquer ||cptfor>0||cpttq>0)){
            container.addView(viewclone);//On ajoute notre clone à la vue
            ScrollView k= (ScrollView) container.getParent();
            k.scrollTo(0,k.getScrollY()+137*2);//on scroll la vue afin de voir le dernier élément posé
            color=colorOrNot(color,elem);//on met une couleur sur l'élément si on attend une condition derrière
            elem=false;
            //on récupère l'élément que l'on a ajouté via son id
            findViewById(R.id.nouv).setOnTouchListener(new MyTouchListener());
            //On modifie son id de lui donné celui de son "type"
            findViewById(R.id.nouv).setId(view.getId());
            bloquer=false;
        }
        return elem;
    }

}
