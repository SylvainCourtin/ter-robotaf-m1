package orleans.info.fr.robotaf.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;


import orleans.info.fr.robotaf.R;
import orleans.info.fr.robotaf.activities.net.TCPClient;

/**
 * Created by Romain on 01/04/2017.
 */

public class ControlActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageButton left, right, move, back, stop;
    private ImageButton klaxoon,  led;


    @Override
    protected void onCreate(Bundle savedInstanceSatate){
        super.onCreate(savedInstanceSatate);
        setContentView(R.layout.activity_control);

        //on récupère les ImageButtons via leur idée
        left = (ImageButton)findViewById(R.id.left);
        right = (ImageButton)findViewById(R.id.right);
        move = (ImageButton)findViewById(R.id.move);
        back = (ImageButton)findViewById(R.id.back);
        stop = (ImageButton)findViewById(R.id.stop);
        klaxoon =(ImageButton) findViewById(R.id.klaxoon);
        led =(ImageButton) findViewById(R.id.led);

        //On initialise le clic
        left.setOnClickListener(this);
        right.setOnClickListener(this);
        move.setOnClickListener(this);
        back.setOnClickListener(this);
        stop.setOnClickListener(this);
        klaxoon.setOnClickListener(this);
        led.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //selon le logo sur lequel on a cliqué
            case R.id.left://
                Log.v("Label","Tourner à gauche");
                RoboTaf.tcpClient.send("gauche()");
                RoboTaf.commandSendRecv.add(">>>gauche()");
                break;
            case R.id.right:
                Log.v("Label","Tourner à droite");
                RoboTaf.tcpClient.send("droite()");
                RoboTaf.commandSendRecv.add(">>>droite()");
                break;
            case R.id.move:
                Log.v("Label","Avancer");
                RoboTaf.tcpClient.send("avancer()");
                RoboTaf.commandSendRecv.add(">>>avancer()");
                break;
            case R.id.back:
                Log.v("Label","Reculer");
                RoboTaf.tcpClient.send("reculer()");
                RoboTaf.commandSendRecv.add(">>>reculer()");
                break;
            case R.id.klaxoon:
                Log.v("Label","Kaxooner");
                RoboTaf.tcpClient.send("klaxon()");
                RoboTaf.commandSendRecv.add(">>>klaxon()");
                break;
            case R.id.stop:
                Log.v("Label","Stoper");
                RoboTaf.tcpClient.send("arreter()");
                RoboTaf.commandSendRecv.add(">>>arreter()");
                break;
            case R.id.led:
               Log.v("Label","Led");
                break;

        }
    }
}
