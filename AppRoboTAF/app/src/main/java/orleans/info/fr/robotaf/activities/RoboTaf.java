package orleans.info.fr.robotaf.activities;

import java.util.LinkedList;
import java.util.List;

import orleans.info.fr.robotaf.activities.net.TCPClient;

/**
 * Created by David on 20/05/2017.
 */

public class RoboTaf {

    public static String ipAddress = "192.168.4.1";
    public static int port = 8080;

    // variable global
    public static TCPClient tcpClient;

    // terminal
    public static List<String> commandSendRecv;

    static {
        tcpClient = new TCPClient(new TCPClient.Recv() {
            @Override
            public void recv(String str) {
                // on ajoute dans la liste les messages reçu (pour le terminal)
                commandSendRecv.add(str);
            }
        });
        commandSendRecv = new LinkedList<String>();
    }
}
