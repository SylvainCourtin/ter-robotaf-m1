-- D1 is connected to green led
-- D2 is connected to blue led
-- D3 is connected to red led
dutyMin = 0
dutyMax = 512
freq = 200

pwm.setup(1, freq, 512)
pwm.setup(2, freq, 512)
pwm.setup(3, freq, 512)
pwm.start(1)
pwm.start(2)
pwm.start(3)

function led(g, r, b)
    pwm.setduty(1, g)
    pwm.setduty(2, r)
    pwm.setduty(3, b)
end
function led_change_moins(pin_couleur)
    local couleur = pwm.getduty(pin_couleur)
    --print("couleur : ",couleur)
    if couleur > dutyMin then return couleur - 1 else return dutyMin end
end
function led_change_plus(pin_couleur)
    local couleur = pwm.getduty(pin_couleur)
    --print("couleur : ",couleur)
    if couleur < dutyMax then return couleur + 1 else return dutyMax end
end
function guirlande(pin_start, nb_tour, delay)
    guirlande_start(pin_start, delay)
    local i = pin_start
    local fin = 3 * nb_tour + pin_start
    while i < fin do
        local round = i % 3
        if round == 1 then -- vert vers rouge
            print("vg")
            while pwm.getduty(1) > dutyMin or pwm.getduty(2) < dutyMax do
                tmr.delay(delay)
                pwm.setduty(1,led_change_moins(1))
                pwm.setduty(2,led_change_plus(2))
            end
        elseif round == 2 then -- rouge vers bleu
            print("gb")
            while pwm.getduty(2) > dutyMin or pwm.getduty(3) < dutyMax do
                tmr.delay(delay)
                pwm.setduty(2,led_change_moins(2))
                pwm.setduty(3,led_change_plus(3))
            end
        elseif round == 0 then -- bleu vers rouge
            print("gr")
            while pwm.getduty(1) < dutyMax or pwm.getduty(3) > dutyMin do
                tmr.delay(delay)
                pwm.setduty(3,led_change_moins(3))
                pwm.setduty(1,led_change_plus(1))
            end
        end
        i = i + 1
        print(i)
    end
    print("go fin")
    guirlande_stop(delay)
end
function guirlande_start(mode, delay)
     while pwm.getduty(mode) < dutyMax do
        tmr.delay(delay)
        pwm.setduty(mode,led_change_plus(mode))            
     end
     print("go")
end
function guirlande_stop(delay)
    print("go go go")
    print(pwm.getduty(1))
    print(pwm.getduty(2))
    print(pwm.getduty(3))
    while pwm.getduty(1) > dutyMin or pwm.getduty(2) > dutyMin or pwm.getduty(3) > dutyMin do
        tmr.delay(delay)
        pwm.setduty(1,led_change_moins(1))
        pwm.setduty(2,led_change_moins(2))
        pwm.setduty(3,led_change_moins(3))
    end
end
   
print("hello word")
led(0, 0, 0)