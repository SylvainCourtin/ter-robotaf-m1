package com.robotaf.robotaf;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import static android.widget.Toast.*;

public class MainActivity extends AppCompatActivity {

    Boolean b1, bwifi;

    //reference aux ressources gérées par l'application
    Context context;

    //pour manager tous les aspects de la connectivité WIFI
    static WifiManager wifi_manager;
    WifiConfiguration wifi_conf;

    byte[] buffer = new byte[1024];

    public static String networkSSID = "esp8266";
    public static String networkPass = "esp8266";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = bwifi = true;
        context = this;

        // Ddétecter les accès disque ou réseauxd ans le thread principal ainsi que les fuites de mémoire. Les
        // accès réseaux peuvent prendre du temps donc on le smet dans unthread à part.
        StrictMode.ThreadPolicy police = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(police);

    }


    //Ici on gère la connexion WIFI
    public void connexionWifi(View ma_vue) {
        // On récupère le service WiFi d'Android
        wifi_manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);


        if (bwifi) {
            //La Wifi est désactivée: on l'active
            activerWifi(context, bwifi);
            bwifi = false;
            //afficher un message
            makeText(getApplicationContext(), "wifi activé", LENGTH_SHORT).show();


            //Toute les étapes de la configuration WIFI
            WifiConfiguration config = new WifiConfiguration();
            config.SSID = "\"" + networkSSID + "\"";
            config.preSharedKey = "\"" + networkPass + "\"";
            config.status = WifiConfiguration.Status.ENABLED;

            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);

            int netid = wifi_manager.addNetwork(config);
            wifi_manager.disconnect();
            wifi_manager.enableNetwork(netid, true);
            wifi_manager.reconnect();
        } else {
            activerWifi(context, bwifi);
            bwifi = true;

            //afficher un message
            Toast.makeText(getApplicationContext(), "wifi désactivé", Toast.LENGTH_SHORT).show();

        }


    }

    public static void activerWifi(Context context, boolean button) {
        wifi_manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi_manager.setWifiEnabled(button);
    }


    public void lampe(View ma_vue) {
        if (b1) {
            b1 = false;
            Envoi mon_envoi = new Envoi();
            buffer = null;
            buffer = ("13").getBytes();
            mon_envoi.run();
            Toast.makeText(MainActivity.this, "Lampe allumée", Toast.LENGTH_SHORT).show();
        } else {

            b1 = true;
            Envoi a = new Envoi();
            buffer = null;
            buffer = ("01").getBytes();
            a.run();
            Toast.makeText(MainActivity.this, "Lampe éteinte", Toast.LENGTH_SHORT).show();
        }
    }

    public class Envoi implements Runnable {
        //Renseigner l'adr publique du serveur et le port
        private final static String SERVER_ADDRESS = "ici";
        private final static int SERVER_PORT = 0;


        public void run() {

            InetAddress serverAddr;
            DatagramPacket packet;

            //représente un socket pour l'envoi et la reception de datagram socket
            DatagramSocket socket;


            try {
                serverAddr = InetAddress.getByName(SERVER_ADDRESS);
                socket = new DatagramSocket();

                // Donnée chargée avec les informations, envoyées sur l'adresse et le numéro de port
                packet = new DatagramPacket(buffer, buffer.length, serverAddr, SERVER_PORT);

                socket.send(packet);
                //On ferme le socket avant un nouvel envoi
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
